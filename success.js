const spanText = document.getElementById("text");
const email = sessionStorage.getItem('email');

if (email) {
  spanText.textContent = email
}

function goToIndex() {
  window.location.href = 'index.html';
}