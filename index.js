const btnSubscribe = document.querySelector('.btn-subscribe');
const form = document.querySelector('form#form');
const emailInp = document.getElementById('email');

form.addEventListener(
  'submit', (event) => {
    event.preventDefault();

    const email = emailInp.value;

    if (validateEmail(email)) {
      sessionStorage.setItem('email', email);
      const encodedEmail = encodeURIComponent(email);
      window.location.href = `success.html?email=${encodedEmail}`;
    } else {
      const emError = document.querySelector('em');
      emError.textContent = "Es requerido un correo válido";
      emailInp.style.border = "1px solid var(--color-primary-tomato)";
      emailInp.style.color = "var(--color-primary-tomato)";
      emailInp.style.backgroundColor = "hsla(4, 85%, 79%, 0.644)";
    }
  }
)

function validateEmail(email) {
  const regex = /^\S+@\S+\.\S+$/;
  return regex.test(email);
}